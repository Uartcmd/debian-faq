# Translation of debian-faq into Dutch.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq_8.1_\n"
"POT-Creation-Date: 2019-07-06 14:47+0200\n"
"PO-Revision-Date: 2017-11-13 14:37+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#. type: Content of: <chapter><title>
#: en/software.dbk:8
msgid "Software available in the Debian system"
msgstr "In het Debian-systeem aanwezige software"

#. type: Content of: <chapter><section><title>
#: en/software.dbk:9
msgid ""
"What types of applications and development software are available for "
"&debian;?"
msgstr ""
"Welke types toepassingssoftware en ontwikkelingssoftware zijn beschikbaar in "
"&debian;?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:11
msgid "Like most Linux distributions, &debian; provides:"
msgstr "Zoals de meeste Linux-distributies levert &debian;:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:16
msgid ""
"the major GNU applications for software development, file manipulation, and "
"text processing, including gcc, g++, make, texinfo, Emacs, the Bash shell "
"and numerous upgraded Unix utilities,"
msgstr ""
"de belangrijkste GNU-toepassingen voor softwareontwikkeling, "
"bestandsmanipulatie en tekstbehandeling, waaronder gcc, g++, make, texinfo, "
"Emacs, de Bash shell en talloze opgewaardeerde Unix-hulpprogramma's,"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:23
msgid ""
"Perl, Python, Tcl/Tk and various related programs, modules and libraries for "
"each of them,"
msgstr ""
"Perl, Python, Tcl/Tk en talrijke verwante programma's, modules en "
"bibliotheken voor elk van hen,"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:29
msgid "TeX (LaTeX) and Lyx, dvips, Ghostscript,"
msgstr "TeX (LaTeX) en Lyx, dvips, Ghostscript,"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:34
msgid ""
"the Xorg windowing system, which provides a networked graphical user "
"interface for Linux, and countless X applications including the GNOME, KDE "
"and Xfce desktop environments,"
msgstr ""
"het windowssysteem van Xorg dat voorziet in een genetwerkte grafische "
"gebruikersinterface voor Linux en talloze grafische applicaties, waaronder "
"de desktopomgevingen GNOME, KDE en Xfce,"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:41
msgid ""
"a full suite of networking applications, including servers for Internet "
"protocols such as HTTP (WWW), FTP, NNTP (news), SMTP and POP (mail) and DNS "
"(name servers); relational databases like PostgreSQL, MySQL; also provided "
"are web browsers including the various Mozilla products,"
msgstr ""
"een volledige suite netwerktoepassingen, waaronder servers voor "
"internetprotocollen zoals HTTP (WWW), FTP, NNTP (nieuws), SMTP en POP (mail) "
"en DNS (naamservers); relationele databases zoals PostgreSQL, MySQL; ook "
"webbrowsers werden voorzien, waaronder de verschillende producten van "
"Mozilla,"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:49
msgid ""
"a complete set of office applications, including the LibreOffice "
"productivity suite, Gnumeric and other spreadsheets, WYSIWYG editors, "
"calendars."
msgstr ""
"een volledige set kantoortoepassingen, waaronder de productieve suite van "
"LibreOffice, Gnumeric en andere rekenbladen, WYSIWYG-editors en "
"kalendertoepassingen."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:55
msgid ""
"More than &packages-main; packages, ranging from news servers and readers to "
"sound support, FAX programs, database and spreadsheet programs, image "
"processing programs, communications, net, and mail utilities, Web servers, "
"and even ham-radio programs are included in the distribution.  Other "
"&packages-contrib-nonfree; software suites are available as Debian packages, "
"but are not formally part of Debian due to license restrictions."
msgstr ""
"De distributie bevat meer dan &packages-main; pakketten, gaande van "
"nieuwsservers en -lezers tot ondersteuning voor geluid, FAX-programma's, "
"databanken en rekenbladen, programma's voor beeldbewerking, "
"communicatieprogramma's, toepassingen voor het net en e-mailtoepassingen, "
"webservers en zelfs programma's voor amateurradio. Nog &packages-contrib-"
"nonfree; andere softwaresuites worden als Debian-pakketten ter beschikking "
"gesteld, maar maken omwille van licentierestricties formeel geen deel uit "
"van Debian."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:64
msgid "Who wrote all that software?"
msgstr "Wie schreef al deze software?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:66
msgid ""
"For each package the <emphasis>authors</emphasis> of the program(s) are "
"credited in the file <literal>/usr/share/doc/PACKAGE/copyright</literal>, "
"where PACKAGE is to be substituted with the package's name."
msgstr ""
"Bij elk pakket worden de <emphasis>auteurs</emphasis> van het programma "
"vermeld in het bestand <literal>/usr/share/doc/PAKKET/copyright</literal>, "
"waarbij PAKKET vervangen moet worden door de naam van het pakket."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:71
msgid ""
"<emphasis>Maintainers</emphasis> who package this software for the &debian; "
"system are listed in the Debian control file (see <xref linkend=\"controlfile"
"\"/>) that comes with each package.  The Debian changelog, in <literal>/usr/"
"share/doc/PACKAGE/changelog.Debian.gz</literal>, mentions the people who've "
"worked on the Debian packaging too."
msgstr ""
"De <emphasis>onderhouders</emphasis> die deze software voor het &debian;-"
"systeem verpakken, worden vermeld in het control-bestand van Debian (zie "
"<xref linkend=\"controlfile\"/>) dat bij elk pakket zit. Ook het changelog-"
"bestand van Debian in <literal>/usr/share/doc/PAKKET/changelog.Debian.gz</"
"literal> vermeldt de mensen die werkten aan de verpakking voor Debian."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:79
msgid ""
"How can I get a current list of programs that have been packaged for Debian?"
msgstr ""
"Hoe verkrijg ik een actuele lijst van programma's die verpakt werden voor "
"Debian?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:81
msgid ""
"A complete list is available from any of the <ulink url=\"https://www.debian."
"org/distrib/ftplist\">Debian mirrors</ulink>, in the file <literal>indices/"
"Maintainers</literal>.  That file includes the package names and the names "
"and e-mails of their respective maintainers."
msgstr ""
"Een volledige lijst is te vinden op elk van de <ulink url=\"https://www."
"debian.org/distrib/ftplist\">Debian mirrors</ulink> in het bestand "
"<literal>indices/Maintainers</literal>. Dat bestand bevat de namen van de "
"pakketten en de namen en de e-mailadressen van hun respectieve onderhouders."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:87
msgid ""
"The <ulink url=\"https://packages.debian.org/\">WWW interface to the Debian "
"packages</ulink> conveniently summarizes the packages in each of about "
"thirty \"sections\" of the Debian archive."
msgstr ""
"De <ulink url=\"https://packages.debian.org/\">WWW-interface naar de Debian-"
"pakketten</ulink> vat de pakketten handig samen in de ongeveer dertig "
"\"secties\" van het Debian-archief."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:93
msgid "How can I install a developer's environment to build packages?"
msgstr ""
"Hoe kan ik een ontwikkelingsomgeving installeren voor het bouwen van "
"pakketten?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:95
msgid ""
"If you want to build packages in your Debian system you will need to have a "
"basic development environment, including a C/C++ compiler and some other "
"essential packages.  In order to install this environment you just need to "
"install the <systemitem role=\"package\">build-essential</systemitem> "
"package.  This is a meta-package or place-holder package which depends on "
"the standard development tools one needs to build a Debian package."
msgstr ""
"Indien u op uw Debian-systeem pakketten wilt bouwen, heeft u een basale "
"ontwikkelingsomgeving nodig, met daarin een C/C++-compiler en enkele andere "
"essentiële pakketten. Om deze omgeving te installeren dient u enkel het "
"pakket <systemitem role=\"package\">build-essential</systemitem> te "
"installeren. Dit is een meta-pakket of een dummypakket dat de standaard "
"ontwikkelingsgereedschappen die men nodig heeft om een Debian-pakket te "
"bouwen, als vereiste heeft."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:103
msgid ""
"Some software can, however, need additional software to be rebuilt, "
"including library headers or additional tools such as <command>autoconf</"
"command> or <command>gettext</command>.  Debian provides many of the tools "
"needed to build other software as Debian packages."
msgstr ""
"Voor het opnieuw bouwen van sommige software kan nog bijkomende software "
"nodig zijn, waaronder bibliotheek-headers of extra gereedschappen, zoals "
"<command>autoconf</command> of <command>gettext</command>. Veel van de "
"gereedschappen die nodig zijn om andere software te bouwen, worden door "
"Debian als Debian-pakketten ter beschikking gesteld."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:109
msgid ""
"Finding which software is precisely required can be tricky, however, unless "
"you are planning on rebuilding Debian packages.  This last task is rather "
"easy to do, as official packages have to include a list of the additional "
"software (besides the packages in <systemitem role=\"package\">build-"
"essential</systemitem>) needed to build the package, this is known as "
"<literal>Build-Dependencies</literal>.  To install all the packages needed "
"to build a given source package and then build said source package you can "
"just run:"
msgstr ""
"Uitzoeken welke software exact nodig is, kan evenwel moeilijk zijn, tenzij u "
"van plan bent Debian-pakketten opnieuw te bouwen. Dit laatste is een eerder "
"gemakkelijke taak, vermits officiële pakketten een lijst moeten bevatten van "
"de extra software (behalve de pakketten in <systemitem role=\"package"
"\">build-essential</systemitem>) die nodig is om het pakket te bouwen en "
"welke bekend staat als de <literal>Build-Dependencies</literal>. Om alle "
"pakketten te installeren die nodig zijn om een bepaald bronpakket te bouwen "
"en vervolgens dat pakket te bouwen, voert u gewoon de volgende opdrachten "
"uit:"

#. type: Content of: <chapter><section><screen>
#: en/software.dbk:119
#, no-wrap
msgid ""
"# apt-get build-dep <replaceable>foo</replaceable>\n"
"# apt-get source --build <replaceable>foo</replaceable>\n"
msgstr ""
"# apt-get build-dep <replaceable>foo</replaceable>\n"
"# apt-get source --build <replaceable>foo</replaceable>\n"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:123
msgid ""
"Notice that if you want to build the Linux kernels distributed by Debian you "
"will want to install also the <systemitem role=\"package\">kernel-package</"
"systemitem> package.  For more information see <xref linkend=\"customkernel"
"\"/>."
msgstr ""
"Merk op dat wanneer u de Linux-kernels die door Debian verdeeld worden, wilt "
"bouwen, u ook het pakket <systemitem role=\"package\">kernel-package</"
"systemitem> moet installeren. Raadpleeg voor bijkomende informatie <xref "
"linkend=\"customkernel\"/>."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:130
msgid "What is missing from &debian;?"
msgstr "Wat ontbreekt in &debian;?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:132
msgid ""
"There is a list of packages which still need to be packaged for Debian, the "
"<ulink url=\"https://www.debian.org/devel/wnpp/\">Work-Needing and "
"Prospective Packages list</ulink>."
msgstr ""
"Er bestaat een lijst van pakketten die nog voor Debian verpakt moeten "
"worden, de  <ulink url=\"https://www.debian.org/devel/wnpp/\">Work-Needing "
"and Prospective Packages list</ulink>."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:137
msgid ""
"For more details about adding missing things, see <xref linkend="
"\"contributing\"/>."
msgstr ""
"Raadpleeg <xref linkend=\"contributing\"/> voor bijkomende informatie over "
"het toevoegen van ontbrekende zaken."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:141
msgid ""
"Why do I get \"ld: cannot find -lfoo\" messages when compiling programs? Why "
"aren't there any libfoo.so files in Debian library packages?"
msgstr ""
"Waarom krijg ik bij het compileren van programma's meldingen zoals \"ld: "
"cannot find -lfoo\"? Waarom bevatten bibliotheekpakketten in Debian geen "
"libfoo.so-bestanden?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:143
msgid ""
"Debian Policy requires that such symbolic links (to libfoo.so.x.y.z or "
"similar)  are placed in separate, development packages.  Those packages are "
"usually named libfoo-dev or libfooX-dev (presuming the library package is "
"named libfooX, and X is a whole number)."
msgstr ""
"De beleidsrichtlijn van Debian vereist dat dergelijke symbolische "
"koppelingen (naar libfoo.so.x.y.z of iets dergelijks) in aparte "
"ontwikkelingspakketten geplaatst worden. Deze pakketten heten gewoonlijk "
"libfoo-dev of libfooX-dev (in de veronderstelling dat het bibliotheekpakket "
"libfooX heet en X een geheel getal is)."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:150
msgid "(How) Does Debian support Java?"
msgstr "(Hoe) ondersteunt Debian Java?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:152
msgid ""
"Several <emphasis>free</emphasis> implementations of Java technology are "
"available as Debian packages, providing both Java Development Kits as well "
"as Runtime Environments.  You can write, debug and run Java programs using "
"Debian."
msgstr ""
"Verschillende <emphasis>vrije</emphasis> implementaties van Java-technologie "
"zijn als Debian-pakketten beschikbaar, met daarbij zowel Java Development "
"Kits als Runtime Environments. Met Debian kunt u Java-programma's schrijven, "
"debuggen en uitvoeren."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:157
msgid ""
"Running a Java applet requires a web browser with the capability to "
"recognize and execute it.  Several web browsers available in Debian, such as "
"Mozilla or Konqueror, support Java plug-ins that enable running Java applets "
"within them."
msgstr ""
"Het uitvoeren van een Java-applet vereist een webbrowser die in staat is het "
"te herkennen en uit te voeren. Verschillende web-browsers in Debian, zoals "
"Mozilla en Konqueror, ondersteunen Java plug-ins die het uitvoeren van Java-"
"applets mogelijk maken."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:162
msgid ""
"Please refer to the <ulink url=\"https://www.debian.org/doc/manuals/debian-"
"java-faq/\">Debian Java FAQ</ulink> for more information."
msgstr ""
"Raadpleeg de <ulink url=\"https://www.debian.org/doc/manuals/debian-java-faq/"
"\">Debian Java FAQ</ulink> voor bijkomende informatie."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:168
msgid ""
"How can I check that I am using a Debian system, and what version it is?"
msgstr ""
"Hoe kan ik nagaan dat ik een Debian-systeem gebruik en welke versie het is?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:170
msgid ""
"In order to make sure that your system has been installed from the real "
"Debian base disks, use the"
msgstr ""
"Om er zeker van te zijn dat uw systeem geïnstalleerd werd met behulp van de "
"echte basisschijven van Debian, gebruikt u het commando"

#. type: Content of: <chapter><section><screen>
#: en/software.dbk:174
#, no-wrap
msgid "lsb_release -a\n"
msgstr "lsb_release -a\n"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:177
msgid ""
"command.  It will display the name of the distribution (in Distributor ID "
"field) and the version of the system (in Release and Codename fields).  The "
"following is an example run in a Debian system:"
msgstr ""
"Dat zal de naam van de distributie weergeven (in het veld Distributor ID) en "
"de versie van het systeem (in de velden Release en Codename). In het "
"hiernavolgende voorbeeld wordt het commando op een Debian-systeem uitgevoerd:"

#. type: Content of: <chapter><section><screen>
#: en/software.dbk:182
#, no-wrap
msgid ""
"$ lsb_release -a\n"
"No LSB modules are available.\n"
"Distributor ID: Debian\n"
"Description:    Debian GNU/Linux 7.4 (wheezy)\n"
"Release:    7.4\n"
"Codename:   wheezy\n"
msgstr ""
"$ lsb_release -a\n"
"No LSB modules are available.\n"
"Distributor ID: Debian\n"
"Description:    Debian GNU/Linux 7.4 (wheezy)\n"
"Release:    7.4\n"
"Codename:   wheezy\n"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:190
msgid ""
"You can also check for the existence of <literal>/etc/debian_version</"
"literal> file, which contains a single one-line entry giving the version "
"number of the release, as defined by the package <literal>base-files</"
"literal>."
msgstr ""
"U kunt ook nagaan of het bestand <literal>/etc/debian_version</literal> "
"bestaat. Het bevat één enkele regel met het versienummer van de release, "
"zoals gedefinieerd door het pakket <literal>base-files</literal>."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:195
msgid ""
"Users should be aware, however, that the Debian system consists of many "
"parts, each of which can be updated (almost) independently.  Each Debian "
"\"release\" contains well defined and unchanging contents.  Updates are "
"separately available.  For a one-line description of the installation status "
"of package <literal>foo</literal>, use the command <literal>dpkg --list foo</"
"literal>.  For a more verbose description, use:"
msgstr ""
"Gebruikers moeten echter weten dat het Debian-systeem uit verschillende "
"onderdelen bestaat en dat elk ervan (bijna) onafhankelijk opgewaardeerd kan "
"worden. Elke \"release\" van Debian heeft een duidelijk omschreven en "
"onveranderlijke inhoud. Updates zijn afzonderlijk beschikbaar. Een "
"beschrijving op één enkele regel van de installatietoestand van een pakket "
"<literal>foo</literal>, krijgt u met het commando <literal>dpkg --list foo</"
"literal>. Gebruik voor een meer uitgebreide beschrijving:"

#. type: Content of: <chapter><section><screen>
#: en/software.dbk:203
#, no-wrap
msgid "dpkg --status foo\n"
msgstr "dpkg --status foo\n"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:206
msgid "To view versions of all installed packages, run:"
msgstr ""
"Om de versienummers van alle geïnstalleerde pakketten te zien, geeft u het "
"commando:"

#. type: Content of: <chapter><section><screen>
#: en/software.dbk:209
#, no-wrap
msgid "dpkg -l\n"
msgstr "dpkg -l\n"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:212
msgid ""
"Note that the existence of the program <literal>dpkg</literal> shows that "
"you should be able to install Debian packages on your system.  However, "
"since the program has been ported to many other operating systems and "
"architectures, this is no longer a reliable method of determining if a "
"system is &debian;."
msgstr ""
"Merk op at het bestaan van het programma <literal>dpkg</literal> aantoont "
"dat u in staat zou moeten zijn om op uw systeem Debian-pakketten te "
"installeren. Aangezien het programma echter geschikt gemaakt werd voor veel "
"andere besturingssystemen en architecturen, is dit niet langer een "
"betrouwbare methode om uit te maken of een systeem &debian; is."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:219
msgid "How does Debian support non-English languages?"
msgstr "Hoe ondersteunt Debian andere talen dan het Engels?"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:223
msgid ""
"&debian; is distributed with keymaps for nearly two dozen keyboards, and "
"with utilities (in the <literal>kbd</literal> package) to install, view, and "
"modify the tables."
msgstr ""
"&debian; wordt verdeeld met toetsenbordindelingen voor bijna twee dozijn "
"toetsenborden en met hulpprogramma's (in het pakket <literal>kbd</literal>) "
"om de tabellen te installeren, te bekijken en te wijzigen."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:228
msgid "The installation prompts the user to specify the keyboard he will use."
msgstr ""
"Het installatieprogramma vraagt de gebruiker om op te geven welk toetsenbord "
"hij gebruikt."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:233
msgid ""
"Nearly all of the software in Debian supports UTF-8 as character set.  "
"Legacy character sets, such as ISO-8859-1 or ISO-8859-2, should be "
"considered obsolete."
msgstr ""
"Bijna alle software in Debian ondersteunt de tekenset UTF-8. Vroegere "
"tekensets, zoals ISO-8859-1 en ISO-8859-2, moeten als verouderd beschouwd "
"worden."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:240
msgid ""
"Currently, support for German-, Spanish-, French-, Hungarian-, Italian-, "
"Japanese-, Korean-, Dutch-, Polish-, Portuguese-, Russian-, Turkish-, and "
"Chinese-language manual pages is provided through the <literal>manpages-"
"LANG</literal> packages (where LANG is the two-letter ISO country code).  To "
"access an NLS manual page, the user must set the shell LC_MESSAGES variable "
"to the appropriate string."
msgstr ""
"Momenteel wordt ondersteuning geboden voor man-pagina's in het Duits, "
"Spaans, Frans, Hongaars, Italiaans, Japans. Koreaans, Nederlands, Pools, "
"Portugees, Russisch, Turks en Chinees via de pakketten <literal>manpages-"
"TAAl</literal> (waarbij TAAL de uit twee letters bestaande ISO-landcode is). "
"Om een man-pagina in zijn moedertaal te raadplegen, moet de gebruiker de "
"shell-variabele LC_MESSAGES op de gepaste manier instellen."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/software.dbk:248
msgid ""
"For example, in the case of the Italian-language manual pages, LC_MESSAGES "
"needs to be set to 'italian'.  The <command>man</command> program will then "
"search for Italian manual pages under <literal>/usr/share/man/it/</literal>."
msgstr ""
"Bijvoorbeeld, voor man-pagina's in de Nederlandse taal, moet LC_MESSAGES "
"ingesteld worden op 'Nederlands'. Het programma <command>man</command> zal "
"dan zoeken naar Nederlandse man-pagina's onder <literal>/usr/share/man/nl/</"
"literal>."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:256
msgid "Where is ezmlm/djbdns/qmail?"
msgstr "Waar vindt men ezmlm/djbdns/qmail?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:258
msgid ""
"Dan J. Bernstein used to distribute <ulink url=\"https://cr.yp.to/software."
"html\">all software he has written</ulink> with a restrictive license which "
"does not allow modified binaries to be distributed.  In november 2007 "
"however, Bernstein said \"[...] i have decided to put all of my future and "
"[...] past software into the public domain\".  See <ulink url=\"https://cr."
"yp.to/distributors.html\">FAQ from distributors</ulink> for his distribution "
"terms."
msgstr ""
"Dan J. Bernstein verspreidde <ulink url=\"https://cr.yp.to/software.html"
"\">alle software die hij schreef</ulink> met een restrictieve licentie die "
"het verspreiden van gewijzigde binaire bestanden verbood. In november 2007 "
"echter verklaarde Bernstein \"[...] ik heb beslist om al mijn toekomstige en "
"[...] mijn vroegere software in het publieke domein te plaatsen\". Zie "
"<ulink url=\"https://cr.yp.to/distributors.html\">FAQ vanwege distributeurs</"
"ulink> voor zijn distributiebepalingen."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:267
msgid ""
"As of this writing (2016-03), <systemitem role=\"package\">ezmlm-idx</"
"systemitem> is available in experimental only (<systemitem role=\"package"
"\">mlmmj</systemitem> is similar, and shipped with Debian jessie); "
"<systemitem role=\"package\">djbdns</systemitem> is available in sid "
"(unstable)  only, see <ulink url=\"https://bugs.debian.org/516394\">Bug "
"#516394</ulink> and <ulink url=\"https://bugs.debian.org/796118\">Bug "
"#796118</ulink> for details and see <systemitem role=\"package\">dbndns</"
"systemitem> for a similar alternative; the <literal>publicfile</literal> "
"software is still not free software, a <systemitem role=\"package"
"\">publicfile-installer</systemitem> package is available from Debian's "
"contrib section."
msgstr ""
"Sinds deze schriftelijke verklaring (03-2016) is <systemitem role=\"package"
"\">ezmlm-idx</systemitem> enkel in 'experimental', de experimentele "
"distributie, te vinden (<systemitem role=\"package\">mlmmj</systemitem> is "
"vergelijkbaar en wordt verdeeld met Debian Jessie). <systemitem role="
"\"package\">djbdns</systemitem> is enkel te vinden in sid (unstable - de "
"onstabiele distributie); zie <ulink url=\"https://bugs.debian."
"org/516394\">Bug #516394</ulink> en <ulink url=\"https://bugs.debian."
"org/796118\">Bug #796118</ulink> voor meer gegevens en zie <systemitem role="
"\"package\">dbndns</systemitem> voor een vergelijkbaar alternatief. De "
"software <literal>publicfile</literal> is nog steeds geen vrije software; "
"het pakket <systemitem role=\"package\">publicfile-installer</systemitem> is "
"te vinden en de sectie 'contrib' van Debian."

#. type: Content of: <chapter><section><para>
#: en/software.dbk:279
msgid ""
"Other software of Dan J. Bernstein (<systemitem role=\"package\">qmail</"
"systemitem>, <systemitem role=\"package\">daemontools</systemitem>, "
"<systemitem role=\"package\">ucspi-tcp</systemitem>) is shipped with Debian."
msgstr ""
"Andere software van Dan J. Bernstein (<systemitem role=\"package\">qmail</"
"systemitem>, <systemitem role=\"package\">daemontools</systemitem>, "
"<systemitem role=\"package\">ucspi-tcp</systemitem>) wordt door Debian "
"verdeeld."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:286
msgid "Where is a player for Flash (SWF)?"
msgstr "Waar vindt men een speler voor Flash (SWF)?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:288
msgid ""
"Debian ships both <systemitem role=\"package\">gnash</systemitem> and "
"<systemitem role=\"package\">swfdec</systemitem>: two free SWF movie players."
msgstr ""
"Debian verdeelt zowel <systemitem role=\"package\">gnash</systemitem> als "
"<systemitem role=\"package\">swfdec</systemitem>: twee vrije SWF filmspelers."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:293
msgid "Where is Google Earth?"
msgstr "Waar is Google Earth te vinden?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:295
msgid ""
"Google Earth is available for GNU/Linux from Google's web site, but not only "
"it is not Free Software, but is completely undistributable by a third "
"party.  However, <systemitem role=\"package\">googleearth-package</"
"systemitem> (in the contrib-section) might be helpful in using this software."
msgstr ""
"Google Earth voor GNU/Linux is te vinden op de website van Google, maar het "
"is niet enkel niet-vrije software, maar mag ook helemaal niet door derden "
"verdeeld worden. Het pakket <systemitem role=\"package\">googleearth-"
"package</systemitem> (in de sectie contrib) kan eventueel behulpzaam zijn "
"bij het gebruik van die software."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:302
msgid "Where is VoIP software?"
msgstr "Waar vindt men VoIP-software?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:304
msgid ""
"Two main open protocols are used for Voice Over IP: SIP and H.323.  Both are "
"implemented by a wide variety of software in Debian main.  <systemitem role="
"\"package\">ekiga</systemitem> is one of the popular clients."
msgstr ""
"Voor Voice Over IP worden twee dominante open protocollen gebruikt: SIP en "
"H.323. Beide worden door een hele resem software in 'Debian main', het "
"hoofdarchief van Debian, geïmplementeerd. <systemitem role=\"package"
"\">ekiga</systemitem> is een van de populaire clients."

#. type: Content of: <chapter><section><title>
#: en/software.dbk:310
msgid ""
"I have a wireless network card which doesn't work with Linux. What should I "
"do?"
msgstr ""
"Ik heb een kaart voor draadloos netwerk die in Linux niet werkt. Wat moet ik "
"doen?"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:312
msgid "Buy one which does :)"
msgstr "Koop er een die wel werkt :)"

#. type: Content of: <chapter><section><para>
#: en/software.dbk:315
msgid ""
"Alternatively, use <systemitem role=\"package\">ndiswrapper</systemitem> to "
"use a driver for Windows (if you have one) on your Linux system.  See the "
"<ulink url=\"https://wiki.debian.org/NdisWrapper\">Debian Wiki ndiswrapper "
"page</ulink> for more information."
msgstr ""
"In het andere geval kunt u <systemitem role=\"package\">ndiswrapper</"
"systemitem> gebruiken om een stuurprogramma voor Windows (als u er een "
"heeft) te gebruiken o uw Linux-systeem. Zie de <ulink url=\"https://wiki."
"debian.org/NdisWrapper\">Debian Wiki-pagina over ndiswrapper</ulink> voor "
"meer informatie."
