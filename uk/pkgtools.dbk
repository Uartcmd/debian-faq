<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="pkgtools"><title>Інструменти для керування пакунками Debian</title>
<section id="pkgprogs"><title>Які програми пропонує Debian для керування пакунками?</title>
<para>
Для керування пакунками Debian існують
численні утиліти, від графічних та
псевдографічних інтерфейсів до
низькорівневих інструментів, що
використовуються для встановлення
пакунків.  Всі доступні утиліти базуються
на низькорівневих інструментах та
перераховуються тут по мірі зростання
їхньої складності.
</para>
<para>
Важливо зрозуміти, що інструменти вищого
рівня, як <command>aptitude</command> чи <command>dselect</command>
залежать від <command>apt</command>, котра, у свою
чергу, залежить від <command>dpkg</command>.
</para>
<para>
Перегляньте <ulink
url="http://www.debian.org/doc/manuals/apt-howto/">APT HOWTO</ulink>, якщо
бажаєте дізнатись більше про інструменти
керування пакунками Debian.  Цей документ є
доступним різними мовами та в різних
форматах, зверніться до запису про APT HOWTO на
сторінці проекту документації Debian <ulink
url="http://www.debian.org/doc/user-manuals#apt-howto">Огляд
підручників для користувачів</ulink>.
</para>
<section id="dpkg"><title>dpkg</title>
<para>
Це основна програма керування пакунками.
<command>dpkg</command> можна викликати з великою
кількістю опцій.  Найчастіше вживаються
такі:
</para>
<itemizedlist>
<listitem>
<para>
Отримати список опцій: <literal>dpkg --help</literal>.
</para>
</listitem>
<listitem>
<para>
Надрукувати файл control (та іншу інформацію)
для заданого пакунку: <literal>dpkg --info
foo_VVV-RRR.deb</literal>
</para>
</listitem>
<listitem>
<para>
Встановити пакунок (включаючи
розпакування та конфігурування) у файлову
систему на жорсткому диску: <literal>dpkg --install
foo_VVV-RRR.deb</literal>.
</para>
</listitem>
<listitem>
<para>
Розпакувати (але не конфігурувати)
архівний файл Debian у файлову систему на
жорсткому диску: <literal>dpkg --unpack
foo_VVV-RRR.deb</literal>.  Зауважте, що ця команда
<emphasis>не</emphasis> обов'язково робить пакунок
готовим до вжитку; деякі файли можуть
потребувати наступного налаштування для
правильного запуску.  Ця команда видаляє
вже встановлену версію програми та
запускає зв'язаний з пакунком сценарій preints
(див.  <xref linkend="maintscripts"/>).
</para>
</listitem>
<listitem>
<para>
Конфігурувати пакунок, що був попередньо
розпакований: <literal>dpkg --configure foo</literal>.  Окрім
іншого, ця команда запускає сценарій postinst
(див.  <xref linkend="maintscripts"/>).  Вона також оновлює
файли, перелічені у <literal>conffiles</literal>.
Зауважте, що операція configure приймає в
якості аргументу назву пакунку, а
<emphasis>не</emphasis> назву архівного файлу Debian
(для прикладу, правильно буде вказувати foo,
а не foo_VVV-RRR.deb).
</para>
</listitem>
<listitem>
<para>
Видобути файл "blurf" (або групу файлів "blurf*") з
архівного файлу Debian: <literal>dpkg --fsys-tarfile
foo_VVV-RRR.deb | tar -xf - blurf*</literal>.
</para>
</listitem>
<listitem>
<para>
Видалити пакунок (але не його
конфігураційні файли): <literal>dpkg --remove foo</literal>.
</para>
</listitem>
<listitem>
<para>
Видалити пакунок (включно з
конфігураційними файлами): <literal>dpkg --purge
foo</literal>.
</para>
</listitem>
<listitem>
<para>
Перелічити стан встановлення пакунків, що
містять послідовність символів (чи
регулярний вираз) foo*: <literal>dpkg --list 'foo*'</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="apt-get"><title>APT</title>
<para>
APT — це вдосконалений пакунковий
інструмент (<emphasis>Advanced Package Tool</emphasis>), що
постачає програму <command>apt-get</command>.
<command>apt-get</command> забезпечує простий шлях для
отримання та встановлення пакунків з
декількох джерел за допомогою командного
рядка.  На відміну від <command>dpkg</command>,
<command>apt-get</command> працює не з .deb-файлами, а з
назвами пакунків і може встановлювати їх
лише з місць, котрі перелічені у файлі
<filename>/etc/apt/sources.list</filename>.  <command>apt-get</command>
викликає <command>dpkg</command> відразу після
завантаження архівного .deb-файлу з
сконфігурованих джерел.  <footnote><para> Зверніть
увагу, що існують портування, котрі
дозволяють використовувати цю утиліту з
іншими системами управління пакунками,
наприклад, менеджером пакунків Red Hat,
відомого як <command>rpm</command> </para> </footnote>.
</para>
<para>
Ось деякі стандартні методи використання
<command>apt-get</command>:
</para>
<itemizedlist>
<listitem>
<para>
Щоб оновити список пакунків, відомих
системі, запустіть:
</para>
<screen>
apt-get update
</screen>
<para>
(вам потрібно запускати цю команду
регулярно для оновлення списку пакунків)
</para>
</listitem>
<listitem>
<para>
Щоб оновити всі пакунки у вашій системі,
наберіть:
</para>
<screen>
apt-get upgrade
</screen>
</listitem>
<listitem>
<para>
Щоб встановити пакунок <replaceable>foo</replaceable>
разом з усіма залежностями, виконайте:
</para>
<screen>
apt-get install foo
</screen>
</listitem>
<listitem>
<para>
Щоб видалити пакунок foo з вашої системи,
введіть:
</para>
<screen>
apt-get remove foo
</screen>
</listitem>
<listitem>
<para>
Щоб видалити пакунок foo разом з його
конфігураційними файлами, наберіть:
</para>
<screen>
apt-get --purge remove foo
</screen>
</listitem>
<listitem>
<para>
Щоб оновити всі пакунки у вашій системі до
нової версії Debian GNU/Linux, запустіть:
</para>
<screen>
apt-get dist-upgrade
</screen>
</listitem>
</itemizedlist>
<para>
Зверніть увагу, що ви повинні бути
зареєстровані як root щоб мати можливість
виконувати будь-які команди, котрі
змінюють системні пакунки.
</para>
<para>
Набір інструментів apt включає також
утиліту <command>apt-cache</command> для запитів
списків пакунків.  Ви можете
використовувати її щоб знайти пакунки,
котрі відповідають певним вимогам за
допомогою простих текстових запитів чи
регулярних виразів, або ж запитів щодо
залежностей у системі керування пакунками.
Ось деякі типові способи використання
<command>apt-cache</command>:
</para>
<itemizedlist>
<listitem>
<para>
Знайти пакунки, чий опис містить
<replaceable>слово</replaceable>:
</para>
<screen>
apt-cache search <replaceable>слово</replaceable>
</screen>
</listitem>
<listitem>
<para>
Роздрукувати детальну інформацію про
пакунок:
</para>
<screen>
apt-cache show <replaceable>назва_пакунка</replaceable>
</screen>
</listitem>
<listitem>
<para>
Вивести список пакунків, від котрих
залежить даний пакунок:
</para>
<screen>
apt-cache depends <replaceable>назва_пакунка</replaceable>
</screen>
</listitem>
<listitem>
<para>
Показати детальну інформацію про доступні
версії даного пакунку та пакунків, від
котрих він залежить:
</para>
<screen>
apt-cache showpkg <replaceable>назва_пакунка</replaceable>
</screen>
</listitem>
</itemizedlist>
<para>
Щоб отримати детальнішу інформацію,
встановіть пакунок <systemitem role="package">apt</systemitem>
та прочитайте
<citerefentry><refentrytitle>apt-get</refentrytitle><manvolnum>8</manvolnum></citerefentry>,
<citerefentry><refentrytitle>sources.list</refentrytitle><manvolnum>5</manvolnum></citerefentry>,
встановіть пакунок <systemitem
role="package">apt-doc</systemitem> та прочитайте
<filename>/usr/share/doc/apt-doc/guide.html/index.html</filename>.
</para>
</section>

<section id="aptitude"><title>aptitude</title>
<para>
<command>aptitude</command> — це керівник пакунків для
системи Debian GNU/Linux, що являє собою зовнішній
інтерфейс до інфраструктури керування
пакунками apt.  <command>aptitude</command> — це програма
з псевдографічним інтерфейсом, що
базується на бібліотеці curses, вона може
використовуватись для швидкого й зручного
керування пакунками.
</para>
<para>
<command>aptitude</command> забезпечує
функціональність <command>dselect</command> і
<command>apt-get</command> та багато додаткових
можливостей, котрих немає у жодній іншій
прогамі:
</para>
<itemizedlist>
<listitem>
<para>
<command>aptitude</command> забезпечує доступ до всіх
версії пакунку.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> веде журнал своїх дій у
файлі <filename>/var/log/aptitude</filename>.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> полегшує відслідковування
застарілого програмного забезпечення
ведучи список „Застарілих та локальних
пакунків“ (Obsolete and Locally Created Packages).
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> є достатньо потужною
системою для пошуку окремих пакунків та
виводу короткої інформації про них.
Користувачі, котрі знайомі з <command>mutt</command>,
швидко засвоять і її, оскільки синтаксис
виразів взято саме звідти.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> відслідковує, які саме
пакунки було встановлено завдяки
залежностям та видаляє їх, якщо пакунки,
які залежали від них, були видалені з
системи.
</para>
</listitem>
<listitem>
<para>
<command>aptitude</command> може автоматично
встановлювати рекомендовані
(<emphasis>Recommended:</emphasis>) пакунки<footnote><para> Не
зважаючи на те, що це часто призводить до
встановлення в систему більшої кількості
пакунків, ніж насправді необхідно для її
роботи.  </para> </footnote>.
</para>
</listitem>
<listitem>
<para>
В повноекранному режимі <command>aptitude</command> має
вбудовану функціональність
суперкористувача та може запускатись
звичайним користувачем.  Вона викличе
програму <command>su</command> (та запитає пароль
суперкористувача, звісно) якщо вам справді
знадобляться привілеї системного
адміністратора.
</para>
</listitem>
</itemizedlist>
<para>
Ви можете працювати з <command>aptitude</command> як
через візуальний інтерфейс (просто
запустивши <literal>aptitude</literal>) так і напряму з
командного рядка.  Синтаксис командного
рядка дуже подібний до синтаксису
<command>apt-get</command>.  Наприклад, щоб встановити
пакунок <replaceable>foo</replaceable>, вам потрібно
запустити <literal>aptitude install
<replaceable>foo</replaceable></literal>.
</para>
<para>
Зверніть увагу, що для встановлення та/або
оновлення вашої системи Debian рекомендує
саме <command>aptitude</command>.
</para>
<para>
Для додаткової інформації прочитайте
сторінку підручника
<citerefentry><refentrytitle>aptitude</refentrytitle><manvolnum>8</manvolnum></citerefentry>
та встановіть пакунок <systemitem
role="package">aptitude-doc-en</systemitem>.
</para>
</section>

<section id="dselect"><title>dselect</title>
<para>
Ця програма являє собою керований у режимі
меню інтерфейс до системи керування
пакунками Debian.  Вона є особливо зручною для
першого встановлення.  Деякі користувачі
можуть почувати себе зручніше у
<command>aptitude</command>, котра також рекомендується
для масштабних оновлень.  Щоб отримати
додаткову інформацію про aptitude див.  <xref
linkend="aptitude"/>.
</para>
<para>
<command>dselect</command> може:
</para>
<itemizedlist>
<listitem>
<para>
бути провідником для користувача, котрий
вирішив встановити чи видалити котрийсь із
пакунків; перевірити, чи не було
встановлено пакунків, що конфліктують з
іншими та чи встановлено всі пакунки, котрі
необхідні для роботи даного пакунка;
</para>
</listitem>
<listitem>
<para>
попередити користувача про суперечливість
чи несумісність його вибору;
</para>
</listitem>
<listitem>
<para>
визначити порядок встановлення пакунків;
</para>
</listitem>
<listitem>
<para>
автоматично виконати встановлення чи
видалення;
</para>
</listitem>
<listitem>
<para>
провести користувача через будь-який
конфігураційний процес, необхідний для
встановлення пакунку.
</para>
</listitem>
</itemizedlist>
<para>
<command>dselect</command> розпочинає свою роботу з
представлення користувачеві меню з 7
елементів, кожен з котрих є наперед
визначеною дією.  Користувач може обрати
будь-яку з них за допомогою стрілок,
пересуваючи підсвітлений рядок та
натиснувши <emphasis>&lt;Enter&gt;</emphasis> щоб вибрати
підсвітлену дію.
</para>
<para>
Що користувач побачить на наступному
екрані залежить від дії, котру він обрав.
Якщо він обрав будь-яку з опцій окрім
<literal>Access</literal> чи <literal>Select</literal>,
<command>dselect</command> просто перейде до виконання
заданої дії: наприклад, якщо користувач
вибрав <literal>Remove</literal>, dselect спробує видалити
всі файли, що вибрані для видалення
користувачем при попередньому
відвідуванні екрану <literal>Select</literal>.
</para>
<para>
Елементи меню <literal>Access</literal> та
<literal>Select</literal> виводять додаткові підменю.
В обидвох випадках меню представлені у
вигляді розділеного екрану: у верхній
частині знаходиться прокручуваний список
можливостей вибору, а в нижній — коротке
пояснення для кожного з елементів верхьої
частини.
</para>
<para>
Додаткова довідка доступна після
натиснення в будь-який момент клавіші '?'.
</para>
<para>
Порядок, в котрому представлені пункти
початкового меню <command>dselect</command>
аналогічний порядку дій користувача коли
він хоче встановити нові пакунки.  Проте
користувач може вибирати будь-який елемент
меню стільки разів, скільки вважатиме за
потрібне (в залежності від того, що він хоче
зробити).
</para>
<itemizedlist>
<listitem>
<para>
Розпочніть з вибору методу доступу (<emphasis
role="strong">Access Method</emphasis>).  Тут вибирається
спосіб, у котрий користувач планує
отримувати пакунки Debian; наприклад, деякі
користувачі мають пакунки на
компакт-диску, тоді як інші планують
отримувати їх з FTP.  Вибір методу доступу
зберігається по завершенню роботи з
<command>dselect</command>, отож вам не потрібно буде
вибирати його знову.
</para>
</listitem>
<listitem>
<para>
Далі оновіть (<emphasis role="strong">Update</emphasis>)
список доступних пакунків.  Щоб зробити це,
<command>dselect</command> зчитує файл „Packages.gz“ ,
котрий повинен знаходитись в теці, де
зберігаються пакунки Debian (Але якщо його там
не виявиться, <command>dselect</command> зможе створити
його для вас).
</para>
</listitem>
<listitem>
<para>
Виберіть (<emphasis role="strong">Select</emphasis>), які
пакунки ви хочете встановити до системи.
Після вибору цього пункту меню користувачу
спочатку виводиться повноекранна довідка
(при умові, що програма запущена без опції
'--expert').  Коли користувач покине вікно
довідки, він побачить розділений надвоє
екран, де він зможе обрати пакунки для
встановлення (чи видалення).
</para>
<para>
Верхня частина екрану — це порівняно
вузьке вікно зі списком 42551 пакунків Debian;
нижня містить опис вибраного
(підсвітленого) пакунку чи групи пакунків.
</para>
<para>
Ви можете вибрати пакунки, з котрими
працюватиме програма, підсвітлюючи назву
пакунку або мітку групи пакунків.  Після
цього ви можете вибирати пакунки:
</para>
<variablelist>
<varlistentry>
<term>для встановлення:</term>
<listitem>
<para>
Це досягається натисненням клавіші '+'.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>для видалення:</term>
<listitem>
<para>
Пакунки можна видаляти двома способами:
</para>
<itemizedlist>
<listitem>
<para>
видаляючи (remove): видаляються більшість
файлів, пов'язаних з пакунком, але
залишаються файли, відмічені як
конфігураційні (див.  <xref linkend="conffile"/>) та
конфігураційна інформація про пакунок.  Це
досягається натисненням клавіші '-'.
</para>
</listitem>
<listitem>
<para>
очищуючи (purged): видаляються <emphasis>всі</emphasis>
файли, котрі відносяться до пакунку.  Це
досягається натисненням клавіші '_'
</para>
</listitem>
</itemizedlist>
<para>
Зверніть увагу, що всі пакунки („All Packages“)
видалити неможливо.  Якщо ви спробуєте
зробити це, у вашій системі залишаться
базові пакунки.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term>для фіксації:</term>
<listitem>
<para>
Це досягається натисненням клавіші '=' і
вказує <command>dselect</command> не оновлювати
пакунок навіть якщо його новіші версії є в
сховищах пакунків Debian, котрі ви
використовуєте (це було визначено, коли ви
вибрали метод доступу до пакунків та
оновились, обравши відповідний пункт меню).
</para>
<para>
Точно так само ви можете відмінити
фіксування пакунку, натиснувши ':'.  В цьому
випадку <command>dselect</command> може оновити
пакунок, якщо буде доступна його новіша
версія.  Така поведінка є стандартною.
</para>
</listitem>
</varlistentry>
</variablelist>
<para>
Ви можете вибирати різний порядок
сортування пакунків, натискаючи клавішу 'o'
для циклічного перемикання між різними
представленнями.  Зазвичай пакунки
сортуються за пріоритетом; всередині
пріоритету пакунки сортуються за теками
(секціями) архіву, у котрому вони
зберігаються.  В такому порядку деякі
пакунки, скажімо, з секції A можуть бути
представлені першими, далі йтимуть якісь
пакунки з секції B, а після них — інші
пакунки з секції A з нижчим пріоритетом.
</para>
<para>
Ви також можете розширити значення міток
вгорі екрану натиснувши клавішу 'v' (verbose —
детально).  Ця дія виведе купу тексту, який
швидше за все не поміститься на екрані.  Щоб
його побачити, натисніть праву стрілку; щоб
повернутись назад — ліву.
</para>
<para>
Якщо ви вибрали пакунок для встановлення
чи видалення, наприклад <systemitem
role="package">foo.deb</systemitem> і цей пакунок залежить
(або рекомендує) інший пакунок, наприклад
<systemitem role="package">blurf.deb</systemitem>, тоді
<command>dselect</command> виведе допоміжне вікно
поверх головного.  Там ви побачите список
залежних пакунків і зможете приймати
запропоновані дії (встановлювати чи ні) або
ж відкидати їх.  Для останнього натисніть
Shift-D; щоб повернутись до попереднього,
натисніть Shift-U.  В будь-якому випадку, ви
можете зберегти свій вибір та повернутись
до головного вікна вибору, натиснувши Shift-Q.
</para>
</listitem>
<listitem>
<para>
Повернувшись до головного меню, користувач
може обрати пункт „Встановлення“ (Install),
щоб розпакувати та сконфігурувати вибрані
пакунки.  Відповідно, користувач, котрий
хоче видалити пакунки, повинен вибрати
пункт меню „Видалити“ (Remove).  І в будь-якому
випадку ви можете вибрати „Вихід“ (Quit), щоб
завершити роботу з <command>dselect</command>; при
цьому все, що ви вибрали (для встановлення,
видалення і т.д.), зберігається.
</para>
</listitem>
</itemizedlist>
</section>

<section id="dpkg-extra"><title>Інші інструменти для керування пакунками</title>
<section id="dpkg-deb"><title>dpkg-deb</title>
<para>
Ця програма маніпулює архівними
(<literal>.deb</literal>) файлами Debian.  Ось її деякі
стандартні опції:
</para>
<itemizedlist>
<listitem>
<para>
Вивести повний список опцій: <literal>dpkg-deb
--help</literal>.
</para>
</listitem>
<listitem>
<para>
Отримати список файлів у архівному файлі:
<literal>dpkg-deb --contents foo_VVV-RRR.deb</literal>
</para>
</listitem>
<listitem>
<para>
Видобути файли з заданого архіву в теку,
визначену користувачем: <literal>dpkg-deb --extract
foo_VVV-RRR.deb tmp</literal> розпаковує всі файли з
архіву <literal>foo_VVV-RRR.deb</literal> до теки
<literal>tmp/</literal>.  Це зручно при перевірці
вмісту пакунка в локальному каталозі без
необхідності встановлення його в кореневу
файлову систему.
</para>
</listitem>
</itemizedlist>
<para>
Зауважте, що всі пакунки, що були лише
розпаковані за допомогою <literal>dpkg-deb
--extract</literal> не є коректно встановленими,
натомість вам потрібно використовувати
<literal>dpkg --install</literal>.
</para>
<para>
Більше ви зможете дізнатись, переглянувши
сторінку довідки
<citerefentry><refentrytitle>dpkg-deb</refentrytitle><manvolnum>1</manvolnum></citerefentry>.
</para>
</section>

<section id="dpkg-split"><title>dpkg-split</title>
<para>
Ця програма ділить великі файли пакунків
на менші (наприклад, для запису на дискети)
та об'єднує набір маленьких файлів назад у
один великий файл.  Вона може
використовуватись лише в Debian-системі
(точніше, в системі, що містить пакунок
<systemitem role="package">dpkg</systemitem>), оскільки вона
викликає програму <literal>dpkg-deb</literal>, щоб
розділити пакунок Debian на складові частини.
</para>
<para>
Наприклад, щоб розділити великий
„.deb“-файл на N частин,
</para>
<itemizedlist>
<listitem>
<para>
Запустіть команду <literal>dpkg-split --split
foo.deb</literal>.  Вона створить N файлів довжиною
приблизно 460 КБайт в поточному каталозі.
</para>
</listitem>
<listitem>
<para>
Скопіюйте ці файли на дискети.
</para>
</listitem>
<listitem>
<para>
Скопіюйте вміст дискет на жорсткий диск на
іншій машині.
</para>
</listitem>
<listitem>
<para>
Об'єднайте шматки файлу командою
<literal>dpkg-split --join "foo*"</literal>.
</para>
</listitem>
</itemizedlist>
</section>

</section>

</section>

<section id="updaterunning"><title>Debian стверджує, що може оновити запущену програму; яким чином він це робить?</title>
<para>
Ядро (файлова система) в Debian GNU/Linux підтримує
заміну файлів, що використовуються.
</para>
<para>
Ми також постачаємо програму під назвою
<command>start-stop-daemon</command>, котра
використовується для запуску демонів під
час завантаження системи, чи їх зупинки під
час зміни рівня запуску ядра (наприклад,
від багатокористувацього режиму в
однокористувацький чи ж при вимкненні).  Та
ж сама програма використовується
встановлювальними сценаріями, коли
встановлюється новий пакунок, що містить
демона: при зупинці запущених демонів та їх
перевантаженні у разі необхідності.
</para>
</section>

<section id="whatpackages"><title>Як мені дізнатись, які пакунки встановлено в системі?</title>
<para>
Щоб взнати стан всіх встановлених пакунків
у Debian-системі, виконайте команду
</para>
<screen>
dpkg --list
</screen>
<para>
Вона виведе однорядкове резюме для кожного
пакунку, включаючи 2-літерний код стану
встановлення (пояснюється у заголовку),
назву пакунку, <emphasis>встановлену</emphasis>
версію та короткий опис.
</para>
<para>
Щоб дізнатись про стан встановлення
пакунків, чиї назви збігаються з набором
літер або виразом, запустіть команду:
</para>
<screen>
dpkg --list 'foo*'
</screen>
<para>
Для отримання детальнішого звіту про
певний пакунок, наберіть команду:
</para>
<screen>
dpkg --status packagename
</screen>
</section>

<section id="filesearch"><title>Як мені дізнатись, що за пакунок встановив котрийсь файл?</title>
<para>
Щоб взнати назву пакунку, що встановив
певний файл виконайте:
</para>
<itemizedlist>
<listitem>
<para>
<literal>dpkg --search filename</literal>
</para>
<para>
Вона шукає файл <literal>filename</literal> серед
встановлених пакунків (на даний момент це
еквівалентно пошуку серед файлів з
розширенням <literal>.list</literal> в теці
<literal>/var/lib/dpkg/info</literal> та виводу тих назв
пакунків, котрі містять заданий файл).
</para>
<para>
Швидшою альтернативою є використання
утиліти <command>dlocate</command>.
</para>
</listitem>
<listitem>
<para>
<literal>zgrep foo Contents-ARCH.gz</literal>
</para>
<para>
Ця команда шукає файли, що містять
підланцюжок <literal>foo</literal> в своій повній
назві.  Файли <literal>Contents-ARCH.gz</literal> (де ARCH —
це назва архітектури) розташовані у
основних каталогах (main, non-free, contrib) на
FTP-сайті Debian (наприклад, в
<literal>/debian/dists/stretch</literal>).  Файли
<literal>Contents</literal> містять дані лише про ті
пакунки, котрі розташовані у їх підтеці.
Таким чином користувачеві може
знадобитись здійснювати пошук не в одному
файлі <literal>Contents</literal>, щоб знайти пакунок,
котрий містить файл <literal>foo</literal>.
</para>
<para>
Цей метод має ту перевагу перед <literal>dpkg
--search</literal>, бо пошук здійснюється навіть у
пакунках, котрі не встановлені у вашій
системі
</para>
</listitem>
<listitem>
<para>
<literal>apt-file search <replaceable>foo</replaceable></literal>
</para>
<para>
Подібно до вищенаведеного, шукаються
файли, що містять ланцюжок чи регулярний
вираз <literal>foo</literal> в повній назві.  Перевага
над попереднім прикладом полягає в тому, що
вам не потрібно отримувати файли
<literal>Contents-ARCH.gz</literal>, оскільки вони
оновлюються автоматично для всіх джерел,
перелічених у <filename>/etc/apt/sources.list</filename> під
час запуску (як root) <literal>apt-file update</literal>.
</para>
</listitem>
</itemizedlist>
</section>

<section id="datapackages"><title>Чому не видаляється foo-data під час видалення foo? Як мені очистити систему від старих пакунків, що вже не використовуються?</title>
<para>
Деякі пакунки розділені на власне програму
(foo) та дані (foo-data).  Це справедливо для
багатьох ігор, мультимедійних додатків та
словників і введено в Debian з тих пір, коли
користувачі захотіли мати доступ до даних
не встановлюючи програму, або ж до
програми, котра б запускалась без даних,
роблячи їх необов'язковими.
</para>
<para>
Подібні ситуації виникають і з
бібліотеками: зазвичай вони
встановлюються, якщо від них залежать
програми з вибраного пакунка.  Коли такий
пакунок видаляться, пакунки з бібліотеками
залишаються в системі.  Або у випадку, коли
пакунок з програмою перестає бути залежним
від пакунка з бібліотекою.
</para>
<para>
В цьому випадку foo-data не залежить від foo,
отож, якщо ви видаляєте пакунок foo, foo-data не
буде автоматично видалено більшістю
інструментів для керування пакунками.  Це
саме відбувається при використанні
пакунків з бібліотеками.  Це необхідно ще й
для того, щоб уникнути циклічних
залежностей.  Якщо ви використовуєте
<command>aptitude</command> (див.  <xref linkend="aptitude"/>) у
якості керівника пакунками, він, окрім
іншого, може відслідковувати автоматично
встановлені пакунки та видаляти їх, якщо в
системі не залишилось пакунків, що їх
потребують.
</para>
</section>

</chapter>

