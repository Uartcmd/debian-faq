# Translation of debian-faq into Japanese.
#
# Copyright © Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-faq package.
#
# Translators:
# victory <victory.deb@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: debian-faq 9.0\n"
"POT-Creation-Date: 2019-07-08 12:01+0200\n"
"PO-Revision-Date: 2016-08-28 19:47+0900\n"
"Last-Translator: Takuma Yamada <tyamada@takumayamada.com>\n"
"Language-Team: Japanese <debian-japanese@lists.debian.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Content of: <chapter><title>
#: en/faqinfo.dbk:8
msgid "General information about the FAQ"
msgstr "この FAQ についての一般情報"

#. type: Content of: <chapter><section><title>
#: en/faqinfo.dbk:9
msgid "Authors"
msgstr "著者"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:11
msgid ""
"The first edition of this FAQ was made and maintained by J.H.M. Dassen "
"(Ray)  and Chuck Stickelman.  Authors of the rewritten &debian; FAQ are "
"Susan G. Kleinmann and Sven Rudolph.  After them, the FAQ was maintained by "
"Santiago Vila and, later, by Josip Rodin.  The current maintainer is Javier "
"Fernandez-Sanguino."
msgstr ""
"この FAQ の初版は J.H.M. Dassen (Ray) さんと Chuck Stickelman さんにより書か"
"れ、保守されました。書き直された &debian; FAQ の著者は Susan G. Kleinmann さ"
"んと Sven Rudolph さんです。その後 FAQ は Santiago Vila さん、さらにその後は "
"Josip Rodin さんにより保守されました。現在のメンテナは Javier Fernandez-"
"Sanguino さんです。"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:18
msgid "Parts of the information came from:"
msgstr "情報元の一部:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:23
msgid ""
"The Debian-1.1 release announcement, by <ulink url=\"https://perens.com/"
"\">Bruce Perens</ulink>,"
msgstr ""
"<ulink url=\"https://perens.com/\">Bruce Perens</ulink> さんによる "
"Debian-1.1 リリース発表"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:29
msgid ""
"the Linux FAQ, by <ulink url=\"https://www.chiark.greenend.org.uk/~ijackson/"
"\">Ian Jackson</ulink>,"
msgstr ""
"<ulink url=\"https://www.chiark.greenend.org.uk/~ijackson/\">Ian Jackson</"
"ulink> さんによる Linux FAQ"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:35
msgid ""
"<ulink url=\"&url-lists-debian;\">Debian Mailing Lists Archives</ulink>,"
msgstr ""
"<ulink url=\"&url-lists-debian;\">Debian メーリングリストアーカイブ</ulink>"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:40
msgid ""
"the dpkg programmers' manual and the Debian Policy manual (see <xref linkend="
"\"debiandocs\"/>),"
msgstr ""
"dpkg プログラマーズマニュアル及び Debian ポリシーマニュアル (<xref linkend="
"\"debiandocs\"/> 参照)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:46
msgid "many developers, volunteers, and beta testers,"
msgstr "多数の開発者や有志、ベータテスター"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:51
msgid "the flaky memories of its authors.  :-)"
msgstr "著者たちの当てにならない記憶 :-)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/faqinfo.dbk:56
msgid ""
"and the <ulink url=\"http://KamarajuKusumanchi.github.io/"
"choosing_debian_distribution/choosing_debian_distribution.html\">Choosing a "
"Debian distribution FAQ</ulink>, which Kamaraju Kusumanchi graciously "
"released under the GPL, so it could be included as a new chapter (see <xref "
"linkend=\"choosing\"/>)."
msgstr ""
"<ulink url=\"http://KamarajuKusumanchi.github.io/"
"choosing_debian_distribution/choosing_debian_distribution.html\">Choosing a "
"Debian distribution FAQ</ulink>、これは Kamaraju Kusumanchi さんが快く GPL で"
"リリースしてくれたので新しい章として収録することができました (<xref linkend="
"\"choosing\"/> 参照)"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:65
msgid ""
"The authors would like to thank all those who helped make this document "
"possible."
msgstr "著者はこの文書の実現を支援してくれた全ての人に感謝します。"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:69
msgid ""
"All warranties are disclaimed.  All trademarks are property of their "
"respective trademark owners."
msgstr "一切の保証を放棄します。商標は全てそれぞれの商標所有者に属します。"

#. type: Content of: <chapter><section><title>
#: en/faqinfo.dbk:74
msgid "Feedback"
msgstr "意見"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:76
msgid ""
"Comments and additions to this document are always welcome.  Please send e-"
"mail to <email>doc-debian@packages.debian.org</email>, or submit a wishlist "
"bug report against the <systemitem role=\"package\"><ulink url=\"https://"
"bugs.debian.org/debian-faq\">debian-faq</ulink></systemitem> package."
msgstr ""
"この文書へのコメントや追加は常に歓迎します。<email>doc-debian@packages."
"debian.org</email> にメールを送るか <systemitem role=\"package\"><ulink url="
"\"https://bugs.debian.org/debian-faq\">debian-faq</ulink></systemitem> パッ"
"ケージに対して wishlist でバグ報告を提出してください。"

#. type: Content of: <chapter><section><title>
#: en/faqinfo.dbk:84
msgid "Availability"
msgstr "入手先"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:86
msgid ""
"The latest version of this document can be viewed on the Debian WWW pages at "
"<ulink url=\"&url-debian-faq;\"/>."
msgstr ""
"この文書の最新版は <ulink url=\"&url-debian-faq;\"/> の Debian WWW ページで見"
"られます。"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:90
#, fuzzy
#| msgid ""
#| "It is also available for download in plain text, HTML, PostScript and PDF "
#| "formats at <ulink url=\"&url-debian-manuals-faq;\"/>.  Also, there are "
#| "several translations there."
msgid ""
"It is also available for download in plain text, HTML, and PDF formats at "
"<ulink url=\"&url-debian-manuals-faq;\"/>.  Also, there are several "
"translations there."
msgstr ""
"<ulink url=\"&url-debian-manuals-faq;\"/> から平文テキストや HTML、"
"PostScript、PDF 形式でのダウンロードも利用できます。また、翻訳もここに複数あ"
"ります。"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:95
msgid ""
"This document is available in the <systemitem role=\"package\">debian-faq</"
"systemitem> package.  Translations are available in <systemitem role="
"\"package\">debian-faq-de</systemitem>, <systemitem role=\"package\">debian-"
"faq-fr</systemitem> and other packages."
msgstr ""
"この文書は <systemitem role=\"package\">debian-faq</systemitem> パッケージで"
"利用できます。翻訳版は <systemitem role=\"package\">debian-faq-de</"
"systemitem> や <systemitem role=\"package\">debian-faq-fr</systemitem> その他"
"のパッケージが利用できます。"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:101
#, fuzzy
#| msgid ""
#| "The original SGML files used to create this document are also available "
#| "in <systemitem role=\"package\">debian-faq</systemitem>'s source package, "
#| "or in SVN at: <literal>svn://svn.debian.org/svn/ddp/manuals/trunk/debian-"
#| "faq</literal> and <ulink url=\"http://svn.debian.org/viewsvn/ddp/manuals/"
#| "trunk/debian-faq/\"/>."
msgid ""
"The original XML files used to create this document are also available in "
"<systemitem role=\"package\">debian-faq</systemitem>'s source package, or in "
"GIT at: <literal>git@salsa.debian.org:ddp-team/debian-faq.git</literal> and "
"<ulink url=\"https://salsa.debian.org/ddp-team/debian-faq\"/>."
msgstr ""
"この文書の作成に利用した元の SGML ファイルも <systemitem role=\"package"
"\">debian-faq</systemitem> のソースパッケージ、または <literal>svn://svn."
"debian.org/svn/ddp/manuals/trunk/debian-faq</literal> や <ulink url=\"http://"
"anonscm.debian.org/viewvc/ddp/manuals/trunk/debian-faq/\"/> の SVN から利用で"
"きます。"

#. type: Content of: <chapter><section><title>
#: en/faqinfo.dbk:108
msgid "Document format"
msgstr "文書形式"

#. type: Content of: <chapter><section><para>
#: en/faqinfo.dbk:110
#, fuzzy
#| msgid ""
#| "This document was written using the DebianDoc SGML DTD (rewritten from "
#| "LinuxDoc SGML).  DebianDoc SGML systems enables us to create files in a "
#| "variety of formats from one source, e.g. this document can be viewed as "
#| "HTML, plain text, TeX DVI, PostScript, PDF, or GNU info."
msgid ""
"This document was written using the DocBook XML DTD.  This system enables us "
"to create files in a variety of formats from one source, e.g. this document "
"can be viewed as HTML, plain text, TeX DVI, PostScript, PDF, or GNU info."
msgstr ""
"この文書は DebianDoc SGML DTD を使って書かれました (LinuxDoc SGML から書き直"
"されました)。DebianDoc SGML システムにより、1つのソースから様々な形式のファイ"
"ルを作成できるようになります。例えばこの文書は HTML や平文テキスト、TeX DVI、"
"PostScript、PDF、GNU info 形式で見られます。"
