<?xml version='1.0' encoding='utf-8'?>
<!-- -*- DocBook -*- -->
<chapter id="getting"><title>Conseguir e instalar Debian GNU/Linux</title>
<section id="boot-floppies"><title>¿Dónde/cómo puedo conseguir los discos de instalación de Debian?</title>
<para>
Puede conseguir los discos de instalación copiando los ficheros adecuados del
servidor FTP de Debian: <ulink
url="ftp://ftp.debian.org/pub/debian/">ftp://ftp.debian.org/pub/debian/</ulink>
y sus <ulink
url="http://www.debian.org/distrib/ftplist.html">réplicas</ulink>.  Por favor,
siga las instrucciones dadas en la Guía de Instalación.  Está disponible (en
inglés) en los ficheros <literal><ulink
url="ftp://ftp.debian.org/pub/debian/dists/stable/main/disks-i386/current/install.txt">install.txt</ulink></literal>
(Texto) y <literal><ulink
url="ftp://ftp.debian.org/pub/debian/dists/stable/main/disks-i386/current/install.html">install.html</ulink></literal>
(HTML).  Algunos casos especiales se detallan a continuación.
</para>
</section>

<section id="cdrom"><title>¿Cómo consigo e instalo Debian desde un CD-ROM?</title>
<para>
Linux soporta el sistema de ficheros ISO 9660 (CD-ROM) con las extensiones Rock
Ridge (conocidas anteriormente como "High Sierra").  Diversos <ulink
url="http://www.debian.org/distrib/vendors">distribuidores</ulink> ofrecen
Debian en este formato.
</para>
<para>
Atención: Cuando vaya a instalar a partir de CD-ROM, normalmente no es una
buena idea escoger el método de acceso <literal>cdrom</literal> de dselect.
Este método normalmente es muy lento.  Los métodos de acceso
<literal>mountable</literal> y <literal>apt</literal>, por ejemplo, son mucho
mejores para la instalación a partir de CD-ROM.
</para>
</section>

<section id="cdimages"><title>Tengo mi propia grabadora de CDs, ¿hay imágenes de CD disponibles en algún sitio?</title>
<para>
Sí.  Para que a los distribuidores de CD les resulte más fácil proporcionar
discos de alta calidad, existen imágenes de CD oficiales <ulink
url="http://cdimage.debian.org/">aquí</ulink>.
</para>
</section>

<section id="floppy"><title>¿Cómo puedo obtener/instalar Debian a partir de un conjunto de disquetes?</title>
<para>
Copie los paquetes de Debian en disquetes formateados.  Da igual que el formato
sea DOS, el nativo de Linux "ext2", o el de "minix" ; sólo tiene que usar la
orden mount apropiada al formato que esté usando
</para>
<para>
Usar disquetes tiene los siguientes problemas:
</para>
<itemizedlist>
<listitem>
<para>
Nombres cortos de MS-DOS: Si pretende poner ficheros de paquetes de Debian en
disquetes formateados para MS-DOS, se dará cuenta de que sus nombres son
demasiado largos, y no se ajustan a la limitación 8.3 de los nombres de
ficheros en MS-DOS.  Para resolverlo, los desarrolladores de Debian ponen todos
sus paquetes disponibles con nombres en formato 8.3 en subdirectorios "msdos"
aparte ( <literal>stable/msdos-i386/</literal>,
<literal>non-free/msdos-i386/</literal>,
<literal>contrib/msdos-i386/</literal>, y
<literal>development/msdos-i386/</literal>).  Los ficheros de estos
subdirectorios son simplemente enlaces simbólicos hacia los ficheros de
archivo de Debian; sólo se diferencian de los ficheros que hay en
<literal>binary-i386/</literal>, y el resto de directorios, en que sus nombres
son más cortos.
</para>
</listitem>
<listitem>
<para>
Tamaños de fichero grandes: Algunos paquetes tienen más de 1.44 MBytes, y no
cabrán en un solo disquete.  Para resolverlo, use la utilidad <xref
linkend="dpkgsplt"/>, disponible en el directorio <literal>tools</literal> en
<ulink url="ftp://ftp.debian.org/debian/">ftp://ftp.debian.org/debian/</ulink>
y sus <ulink
url="http://www.debian.org/distrib/ftplist.html">réplicas</ulink>.
</para>
</listitem>
</itemizedlist>
<para>
Tiene que tener soporte de disquetes en el núcleo para poder leer y escribir
en disquetes; la mayoría de los núcleos vienen con soporte para disquetes
incluido.
</para>
<para>
Para montar un disquete bajo el nombre <literal>/floppy</literal> (un
directorio que se debe haber creado durante la instalación), use:
</para>
<itemizedlist>
<listitem>
<para>
<literal>mount -t msdos /dev/fd0 /floppy/</literal> si el disquete está en la
unidad A: y tiene formato MS-DOS,
</para>
</listitem>
<listitem>
<para>
<literal>mount -t msdos /dev/fd1 /floppy/</literal> si el disquete está en la
unidad B: y tiene formato MS-DOS,
</para>
</listitem>
<listitem>
<para>
<literal>mount -t ext2 /dev/fd0 /floppy/</literal> si el disquete está en la
unidad A: y tiene formato ext2 (el normal de Linux).
</para>
</listitem>
</itemizedlist>
</section>

<section id="anonftp"><title>¿Cómo puedo conseguir e instalar Debian directamente desde un servidor de FTP?</title>
<para>
Para detalles de cómo instalar un paquete, vea las notas sobre <xref
linkend="dpkg"/>.
</para>
<para>
Luego llame al programa <literal>dselect</literal>, que llamará a
<literal>dpkg-ftp</literal> por usted, le guiará a través de la selección de
paquetes y los instalará, sin que tenga que copiarlos previamente en su
sistema.  Este método está pensado para ahorrar al usuario espacio de disco y
tiempo.  Fíjese en que no se necesita una configuración especial del núcleo
para acceder a paquetes de Debian e instalarlos mediante este método.
</para>
<para>
Para usar este servicio de <literal>dselect</literal>, tendrá que saber:
</para>
<itemizedlist>
<listitem>
<para>
el nombre de dominio del servidor de FTP anónimo que pretenda usar.
</para>
</listitem>
<listitem>
<para>
el directorio que contiene los ficheros que pretende instalar, o los
subdirectorios que contienen dichos ficheros.  Este directorio debe contener un
fichero llamado "Packages" (o su version comprimida, "Packages.gz").
</para>
</listitem>
</itemizedlist>
</section>

<section id="atape"><title>¿Cómo puedo conseguir e instalar Debian desde cinta de streamer?</title>
<para>
De momento, instalar paquetes directamente desde cinta no es posible.  Pero se
puede usar <literal>tar</literal>, <literal>cpio</literal>, o
<literal>afio</literal> para copiar los ficheros de archivo de Debian a cinta,
y luego copiarlos al disco local para instalarlos.  En la misma línea, se
pueden usar disquetes que contengan ficheros "tar" que serán copiados a disco
local antes de ser gestionados por las utilidades de paquetes de Debian.
</para>
</section>

<section id="version"><title>¿Cuál es la última version de Debian?</title>
<para>
Actualmente existen tres versiones de Debian:
</para>
<variablelist>
<varlistentry>
<term><emphasis>9, alias "stable"</emphasis></term>
<listitem>
<para>
Este es software estable, pero puede cambiar cuando se incorporen
modificaciones importantes.
</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis>la distribución `testing'</emphasis> bla bla</term>
<term><emphasis>la distribucion `unstable'</emphasis></term>
<listitem>
<para>
la versión de "unstable" (de desarrollo).  Esta es la versión actualmente en
desarrollo; se actualiza continuamente.  Puede recuperar paquetes del archivo
"unstable" en cualquier servidor de FTP de Debian y usarlos para actualizar su
sistema en cualquier momento.
</para>
<para>
Está planeado que ésta se convierta en una nueva distribución de Debian
varios meses después de la última publicación.
</para>
</listitem>
</varlistentry>
</variablelist>
</section>

</chapter>

